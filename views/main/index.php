<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\models\Users */
/* @var $payment app\models\Payments */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Payment form';
?>
<div class="main-index">

    <div class="body-content">
        <div class="row">
            <div class="col-lg-12">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'class' => 'form'
                    ],
                ]); ?>

                <div class="step" data-number="0">
                    <p class="title">This is step 1</p>
                    <?= $form->field($user, 'first_name')->textInput([
                        'placeholder' => 'First name'
                    ])->label(false) ?>

                    <?= $form->field($user, 'last_name')->textInput([
                        'placeholder' => 'Last name'
                    ])->label(false) ?>

                    <?= $form->field($user, 'phone_number')->textInput([
                        'placeholder' => 'Phone number',
                    ])->label(false) ?>
                </div>

                <div class="step" data-number="1">
                    <p class="title">This is step 2</p>
                    <?= $form->field($user, 'address_street')->textInput([
                        'placeholder' => 'Street'
                    ])->label(false) ?>

                    <?= $form->field($user, 'address_house_number')->textInput([
                        'placeholder' => 'House number'
                    ])->label(false) ?>

                    <?= $form->field($user, 'address_zip_code')->textInput([
                        'placeholder' => 'Zip code',
                    ])->label(false) ?>

                    <?= $form->field($user, 'address_city')->textInput([
                        'placeholder' => 'City'
                    ])->label(false) ?>
                </div>

                <div class="step" data-number="2">
                    <p class="title">This is step 3</p>
                    <?= $form->field($payment, 'iban')->textInput([
                        'placeholder' => 'IBAN'
                    ])->label(false) ?>

                    <?= $form->field($payment, 'account_owner')->textInput([
                        'placeholder' => 'Account owner'
                    ])->label(false) ?>
                </div>

                <p class="form-controls">
                    <a href="#" class="btn btn-info back">Previous step</a>
                    <a href="#" class="btn btn-info next">Next step</a>
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-success', 'id' => 'submit']) ?>
                </p>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    </div>
</div>
