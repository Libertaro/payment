<?php

use yii\helpers\Url;

/* @var $payment_result array */

$this->title = 'Result page';
?>
<div class="main-result">

    <div class="body-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="form">

                    <p class="title">This is step 4</p>

                    <div class="result success">
                        <p class="title">Data successfully saved</p>
                    </div>

                    <?php foreach ($payment_result as $key => $value): ?>
                        <div class="result <?= $key === 'paymentDataId' ? 'success' : 'error' ?>"><?= $key . ': ' . $value ?></div>
                    <?php endforeach; ?>
                    <p class="form-controls">
                        <a id="home" href="<?= Url::home(true); ?>" class="btn btn-info">Repeat</a>
                    </p>

                </div>
            </div>
        </div>

    </div>
</div>
