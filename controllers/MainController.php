<?php

namespace app\controllers;

use app\models\Payments;
use app\models\Users;
use yii\web\Controller;
use Yii;

class MainController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user = new Users();
        $payment = new Payments();

        if ($params = Yii::$app->request->post()) {
            if ($user->load($params) && $user->save()) {
                $payment->user_id = $user->id;
                if ($payment->load($params) && $payment->save()) {
                    $payment_result = $payment->makePayment($payment->account_owner, $payment->iban, $user->id);
                    return $this->render('result', [
                        'payment_result' => $payment_result
                    ]);
                }
            }
        }

        return $this->render('index', [
            'user' => $user,
            'payment' => $payment
        ]);
    }
}
