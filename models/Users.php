<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $address_street
 * @property string $address_house_number
 * @property int $address_zip_code
 * @property string $address_city
 * @property string $phone_number
 *
 * @property Users $user
 */
class Users extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address_zip_code'], 'integer'],
            [['first_name', 'last_name', 'address_street', 'address_house_number', 'address_city', 'phone_number'], 'string', 'max' => 255],
            [['first_name', 'last_name', 'address_street', 'address_house_number', 'address_city', 'phone_number', 'address_zip_code'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'address_street' => Yii::t('app', 'Street'),
            'address_house_number' => Yii::t('app', 'House Number'),
            'address_zip_code' => Yii::t('app', 'Zip Code'),
            'address_city' => Yii::t('app', 'City'),
            'phone_number' => Yii::t('app', 'Phone number'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['user_id' => 'id']);
    }
}
