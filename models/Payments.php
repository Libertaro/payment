<?php

namespace app\models;

use Yii;
use yii\httpclient\Client;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $user_id
 * @property string $account_owner
 * @property string $iban
 * @property string $payment_data_id
 *
 * @property Users $user
 */
class Payments extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['account_owner', 'iban'], 'string', 'max' => 255],
            [['payment_data_id'], 'string', 'max' => 512],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['account_owner', 'iban'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'account_owner' => Yii::t('app', 'Account Owner'),
            'iban' => Yii::t('app', 'Iban'),
            'payment_data_id' => Yii::t('app', 'Payment data ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function makePayment($account_owner, $iban, $user_id)
    {
        $client = new Client();
        $response = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod('post')
            ->setUrl('https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data')
            ->setData([
                'customerId' => $user_id,
                'iban' => $iban,
                'owner' => $account_owner
            ])
            ->send();
        if ($response->isOk) {
            $payment = Payments::findOne(['user_id' => $user_id]);
            $payment->payment_data_id = $response->data['paymentDataId'];
            $payment->update();
            return ['paymentDataId' => $payment->payment_data_id];
        } else {
            switch ($response->statusCode) {
                case '400':
                    $message = ['error' => 'Required parameters are missed. Payment error'];
                    break;
                default:
                    $message = ['error' => 'Payment error. Please try again later'];
            }
        }

        return $message;
    }
}
