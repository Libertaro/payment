<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m181003_103130_create_users_table extends Migration
{

    const USERS_TABLE = 'users';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::USERS_TABLE, [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'address_street' => $this->string(),
            'address_house_number' => $this->string(),
            'address_zip_code' => $this->integer(),
            'address_city' => $this->string(),
            'phone_number' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::USERS_TABLE);
    }
}
