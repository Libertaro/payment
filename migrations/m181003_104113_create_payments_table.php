<?php

use yii\db\Migration;

/**
 * Handles the creation of table `payments`.
 */
class m181003_104113_create_payments_table extends Migration
{

    const PAYMENTS_TABLE = 'payments';
    const USERS_TABLE = 'users';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::PAYMENTS_TABLE, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'account_owner' => $this->string(),
            'iban' => $this->string(),
            'payment_data_id' => $this->string(512),
        ]);

        $this->addForeignKey(
            'fk-payments-user_id',
            self::PAYMENTS_TABLE,
            'user_id',
            self::USERS_TABLE,
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-payments-user_id', self::PAYMENTS_TABLE);
        $this->dropTable(self::PAYMENTS_TABLE);
    }
}
