var steps = $(".form").children(".step");
var current_step = 0;

$(window).on('beforeunload', function () {
    var form_elements = $(".form input");
    form_elements.each(function (i, item) {
        localStorage.setItem($(item).attr('id'), $(item).val());
    });

    localStorage.setItem('current_step', current_step);
});

$(window).on('load', function (e) {
    if (localStorage.getItem('current_step')) {
        current_step = localStorage.getItem('current_step');
    }

    if ($("div .main-result").length > 0) {
        current_step = 0;
    }

    changeStep(current_step);

    $.each(localStorage, function (key, value) {
        $("#" + key).val(value);
    });
});

$("#home").click(function () {
    localStorage.clear();
});

$("a.next").click(function () {
    current_step++;
    changeStep(current_step);
    $("a.back").removeClass('btn-disable');
});

$("a.back").click(function () {
    if (current_step > 0) {
        current_step--;
        changeStep(current_step);
        $("#submit").hide();
        $("a.next").show();
    }
});

function changeStep(i) {
    $(steps).hide();
    $(steps[i]).fadeIn();
    if (i == 2) {
        $("a.next").hide();
        $("#submit").css("display", "inline-block");
    }
    if (i == 0) {
        $("a.back").addClass('btn-disable');
    }
}